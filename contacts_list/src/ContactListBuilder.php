<?php

namespace Drupal\contacts_list;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;


class contactListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['nom'] = $this->t('Nom');
    $header['prenom'] = $this->t('Prénom');
    $header['email'] = $this->t('Email');
    $header['Téléphone'] = $this->t('Téléphone');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['nom'] = Link::createFromRoute(
      $entity->getNom(),
      'entity.contact.canonical',
      ['contact' => $entity->id()]
    );
    $row['prenom'] = $entity->getPrenom();
    $row['email'] = $entity->getEmail();
    $row['Téléphone'] = $entity->getPhone();

    return $row + parent::buildRow($entity);
  }

}
