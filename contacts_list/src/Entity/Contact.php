<?php

namespace Drupal\contacts_list\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the contact entity class.
 *
 * @ContentEntityType(
 *   id = "contact",
 *   label = @Translation("Contact"),
 *   base_table = "contact",
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_list\ContactListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\contacts_list\ContactAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "canonical" = "/contact/{contact}",
 *     "add-form" = "/contact/add",
 *     "edit-form" = "/contact/{contact}/edit",
 *     "delete-form" = "/contact/{contact}/delete",
 *   },
 * )
 */
class Contact extends ContentEntityBase implements ContentEntityInterface {
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    // identifiant unique.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('L\'identifiant de l\'entité'))
      ->setReadOnly(TRUE);
    $fields['nom'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Nom'))
      ->setDescription(t('Le nom du contact'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ));
    $fields['prenom'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Prénom'))
      ->setDescription(t('Le prénom du contact'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ));
    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t('L\'email du contact'))
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 2,
      ));
    $fields['phone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Numéro de téléphone'))
      ->setDescription(t('Le numéro de téléphone du contact'))
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ));
    return $fields;
  }
  public function setNom($nom) {
    $this->set('nom', $nom);
  }
  public function getNom() {
    return $this->get('nom')->value;
  }
  public function setPrenom($prenom) {
    $this->set('prenom', $prenom);
  }
  public function getPrenom() {
    return $this->get('prenom')->value;
  }
  public function setEmail($email) {
    $this->set('email', $email);
  }
  public function getEmail() {
    return $this->get('email')->value;
  }
  public function setPhone($phone) {
    $this->set('phone', $phone);
  }
  public function getPhone() {
    return $this->get('phone')->value;
  }
}


